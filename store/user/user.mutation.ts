import {Mutations} from 'vuex-smart-module';

import {User} from '@/src/app/sample/user/model/user.model';
import {UserState} from '@/store/user/user.state';
import {Mapper} from '@/src/core/service/mapper.service';

export class UserMutation extends Mutations<UserState> {
  mapper = new Mapper();

  setUsers(params: User.Response.FindAll[]) {
    this.state.users = this.mapper.toArray(User.Response.FindAll, params);
  }

  setUser(params: User.Response.FindOne) {
    this.state.user = this.mapper.toObject(User.Response.FindOne, params);
  }
}
