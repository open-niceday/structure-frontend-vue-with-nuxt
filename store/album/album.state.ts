import {Album} from '@/src/app/sample/album/model/album.model';

export class AlbumState {
  albums: Album.Response.FindAll[] = [];
  album: Album.Response.FindOne = new Album.Response.FindOne;
  result: Album.Response.FindOne = new Album.Response.FindOne;
}
