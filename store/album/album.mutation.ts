import {Mutations} from 'vuex-smart-module';

import {Album} from '@/src/app/sample/album/model/album.model';
import {AlbumState} from '@/store/album/album.state';
import {Mapper} from '@/src/core/service/mapper.service';

export class AlbumMutation extends Mutations<AlbumState> {
  mapper = new Mapper();

  setAlbums(params: Album.Response.FindAll[]) {
    this.state.albums = this.mapper.toArray(Album.Response.FindAll, params);
  }

  setAlbum(params: Album.Response.FindOne) {
    this.state.album = this.mapper.toObject(Album.Response.FindOne, params);
  }

  setResult(params: Album.Response.FindOne) {
    this.state.result = this.mapper.toObject(Album.Response.FindOne, params);
  }
}
