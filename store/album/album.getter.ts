import {Getters} from 'vuex-smart-module';

import {Album} from '@/src/app/sample/album/model/album.model';
import {AlbumState} from '@/store/album/album.state';
import {Mapper} from '@/src/core/service/mapper.service';

export class AlbumGetter extends Getters<AlbumState> {
  mapper = new Mapper();

  getAlbums(): Album.Response.FindAll[] {
    return this.mapper.toArray(Album.Response.FindAll, this.state.albums);
  }

  getAlbum(): Album.Response.FindOne {
    return this.mapper.toObject(Album.Response.FindOne, this.state.album);
  }

  getResult(): Album.Response.FindOne {
    return this.mapper.toObject(Album.Response.FindOne, this.state.result);
  }
}
