import {AxiosResponse} from 'axios';
import {Actions} from 'vuex-smart-module';

import {Validate} from '@/src/core/decorator/validate.decorator';
import {Album} from '@/src/app/sample/album/model/album.model';
import {AlbumState} from '@/store/album/album.state';
import {AlbumGetter} from '@/store/album/album.getter';
import {AlbumMutation} from '@/store/album/album.mutation';
import AxiosService from '@/src/app/shared/service/axios.service';

export class AlbumAction extends Actions<AlbumState, AlbumGetter, AlbumMutation, AlbumAction> {
  private http = new AxiosService('https://jsonplaceholder.typicode.com');

  getAlbums() {
    return this.http.get(
      `/users`
    ).then(
      (response: AxiosResponse<Album.Response.FindAll[]>) =>
        this.mutations.setAlbums(response.data)
    );
  }

  getAlbum(id: string) {
    return this.http.get(
      `/albums/${id}`
    ).then(
      (response: AxiosResponse<Album.Response.FindOne>) =>
        this.mutations.setAlbum(response.data)
    );
  }

  @Validate
  setAlbum(params: Album.Request.Add) {
    return this.http.post(
      `/albums`,
      params
    ).then(
      (response: AxiosResponse<Album.Response.FindOne>) =>
        this.mutations.setResult(response.data)
    );
  }
}
