import {Module} from 'vuex-smart-module';

import {AlbumState} from '@/store/album/album.state';
import {AlbumGetter} from '@/store/album/album.getter';
import {AlbumMutation} from '@/store/album/album.mutation';
import {AlbumAction} from '@/store/album/album.action';

export default new Module({
  state: AlbumState,
  getters: AlbumGetter,
  mutations: AlbumMutation,
  actions: AlbumAction,
});
