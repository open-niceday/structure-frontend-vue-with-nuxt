import cookie from 'cookie';
import * as Cookies from 'js-cookie';
import createPersistedState from 'vuex-persistedstate';

export default ({store, req}: any) => {
  createPersistedState({
    key: 'TEST',
    paths: ['global'],
    storage: {
      getItem: (key) => {
        if (process.server) {
          const parsedCookies = cookie.parse(!!req.headers.cookie ? req.headers.cookie : JSON.stringify({}));
          return parsedCookies[key];
        } else {
          return Cookies.get(key);
        }
      },
      setItem: (key, value) =>
        Cookies.set(key, value, {expires: 365, secure: false}),
      removeItem: key => Cookies.remove(key)
    }
  })(store);
};
