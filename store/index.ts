import 'reflect-metadata';
import Vue from 'vue';
import Vuex from 'vuex';
import {Module} from 'vuex-smart-module';

import UserModule from '@/store/user/user.module';
import AlbumModule from '@/store/album/album.module';
import GlobalModule from '@/store/global/global.module';

Vue.use(Vuex);

const root = new Module({
  modules: {
    user: UserModule,
    album: AlbumModule,
    global: GlobalModule
  },
});

export const {
  state,
  getters,
  mutations,
  actions,
  modules,
  plugins
} = root.getStoreOptions();
