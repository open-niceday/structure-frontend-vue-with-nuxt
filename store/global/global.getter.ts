import {Getters} from 'vuex-smart-module';

import {GlobalState} from '@/store/global/global.state';

export class GlobalGetter extends Getters<GlobalState> {
  getToken() {
    return !!this.state.token ? `${this.state.token}` : '';
  }
}
