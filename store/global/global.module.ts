import {Module} from 'vuex-smart-module';

import {GlobalState} from '@/store/global/global.state';
import {GlobalGetter} from '@/store/global/global.getter';
import {GlobalMutation} from '@/store/global/global.mutation';
import {GlobalAction} from '@/store/global/global.action';

export default new Module({
  state: GlobalState,
  getters: GlobalGetter,
  mutations: GlobalMutation,
  actions: GlobalAction
});
