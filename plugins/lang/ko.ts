import SharedKo from '@/src/app/shared/lang/shared.ko';
import AlbumKo from '@/src/app/sample/album/lang/album.ko';

export default {
  shared: SharedKo,
  album: AlbumKo,
  user: '사용자',
}
