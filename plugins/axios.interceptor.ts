import {AxiosError, AxiosRequestConfig, AxiosResponse} from 'axios';

import {AxiosProcessorService} from '@/src/app/shared/service/axios.processor.service';

export default function({$axios}: any) {
  const axiosProcessorService = new AxiosProcessorService();

  $axios.onRequest((config: AxiosRequestConfig) => {
    if (process.server) {
      config = axiosProcessorService.onRequest(config);
    }
    return config;
  });

  $axios.onResponse((response: AxiosResponse) => {
    if (process.server) {
      const message = axiosProcessorService.onResponse(response);
      console.log(message);
    }
    return response;
  });

  $axios.onRequestError((error: AxiosError) => {
    if (process.server) {
      const message = axiosProcessorService.onRequestError(error);
      console.log(message);
      throw new Error(error.code);
    }

    return Promise.resolve(error);
  });

  $axios.onResponseError((error: AxiosError) => {
    if (process.server) {
      const message = axiosProcessorService.onResponseError(error);
      console.log(message);
      throw new Error(error.code);
    }

    return Promise.resolve(error);
  });

  $axios.onError((error: AxiosError) => {
    if (process.server) {
      const message = axiosProcessorService.onError(error);
      console.log(message);
      throw new Error(error.code);
    }

    return Promise.resolve(error);
  });
}
