import Vue from 'vue';

import NSample from '@/src/app/shared/components/sample.vue';
import NSpinner from '@/src/app/shared/components/n-spinner.vue';

export default {
  install() {
    Vue.component('n-sample', NSample);
    Vue.component('n-spinner', NSpinner);
  }
};
