import {Message} from '@/src/app/shared/model/message.model';
import {Broadcast} from '@/src/core/service/broadcast.service';
import CustomError from '@/src/core/model/custom-error';

export default class ErrorHandler {
  broadcast = new Broadcast();

  constructor(code: Message, errors?: any) {
    switch (code) {
      case Message.E001001:
        this.setValidateError(errors);
        break;
      case Message.E001002:
        this.setValidateError(errors);
        break;
    }
  }

  private setValidateError(errors: any) {
    if (process.client) {
      this.broadcast.onValidate(errors);
    } else if (process.server) {
      throw new CustomError(Message.E001001, errors);
    }
  }
}
