import 'reflect-metadata';
import {ClassConstructor, ClassTransformer, plainToClass} from 'class-transformer';

export class Mapper extends ClassTransformer {
  toObject<T>(type: ClassConstructor<T>, source: T): T {
    return plainToClass(type, source);
  }

  toArray<T>(type: ClassConstructor<T>, sources: T[]): T[] {
    return plainToClass(type, sources);
  }
}
