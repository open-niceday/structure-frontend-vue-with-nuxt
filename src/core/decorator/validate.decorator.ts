import _ from 'lodash';
import * as validator from 'class-validator';
import {ValidationError} from 'class-validator';
import {Message} from '@/src/app/shared/model/message.model';
import ErrorHandler from '@/src/core/service/error.handler';

export function Validate(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
  const $validator = new validator.Validator();
  const original = descriptor.value;
  descriptor.value = function(...args: any) {
    return $validator.validate(args[0]).then((errors: ValidationError[]) => {

      if (_.isEmpty(errors)) {
        return original.apply(this, args);
      }

      return new ErrorHandler(Message.E001001, errors);
    });
  };
}
