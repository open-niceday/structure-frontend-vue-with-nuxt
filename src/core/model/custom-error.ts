import {Message} from '@/src/app/shared/model/message.model';

export default class CustomError extends Error {
  code!: Message;
  data!: any;

  constructor(code: Message, data?: any) {
    super(code.toString());
    this.code = code;
    this.data = data;
    this.name = code;
  }
}
