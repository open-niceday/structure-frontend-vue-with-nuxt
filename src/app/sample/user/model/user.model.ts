import 'reflect-metadata';
import {Expose, Type} from 'class-transformer';

import {Description} from '@/src/core/decorator/description.decorator';

export namespace User {
  export namespace Response {
    export class FindAll {
      @Expose() @Description('id')
      id!: number;

      @Expose() @Description('name')
      name!: string;

      @Expose() @Description('username')
      username!: string;

      @Expose() @Description('email')
      email!: string;

      @Expose() @Description('phone')
      phone!: string;

      @Expose() @Description('website')
      website!: string;

      @Expose() @Description('address')
      @Type(() => Address)
      address!: Address;

      @Expose() @Description('company')
      @Type(() => Company)
      company!: Company;
    }

    export class FindOne {
      @Expose() @Description('id')
      id!: number;

      @Expose() @Description('name')
      name!: string;

      @Expose() @Description('username')
      username!: string;

      @Expose() @Description('email')
      email!: string;

      @Expose() @Description('phone')
      phone!: string;

      @Expose() @Description('website')
      website!: string;

      @Expose() @Description('address')
      @Type(() => Address)
      address!: Address;

      @Expose() @Description('company')
      @Type(() => Company)
      company!: Company;
    }

    export class Address {
      @Expose() @Description('street')
      street!: string;

      @Expose() @Description('suite')
      suite!: string;

      @Expose() @Description('city')
      city!: string;

      @Expose() @Description('zipcode')
      zipcode!: string;

      @Expose() @Description('geo')
      @Type(() => Geo)
      geo!: Geo;
    }

    export class Geo {
      @Expose() @Description('lat')
      lat!: string;

      @Expose() @Description('lng')
      lng!: string;
    }

    export class Company {
      @Expose() @Description('name')
      name!: string;

      @Expose() @Description('catchPhrase')
      catchPhrase!: string;

      @Expose() @Description('bs')
      bs!: string;
    }
  }
}
