import {RouteConfig} from '@nuxt/types/config/router';

import Default from '@/src/app/system/layout/default.vue';
import UserList from '@/src/app/sample/user/view/user-list.vue';
import UserDetail from '@/src/app/sample/user/view/user-detail.vue';

const router: RouteConfig[] = [
  {
    path: '/default/user',
    component: Default,
    redirect: '/default/user/list',
    children: [
      {
        path: 'list',
        component: UserList,
      },
      {
        path: 'detail/:id',
        component: UserDetail,
      }
    ]
  }
];

export default router;
