import {RouteConfig} from '@nuxt/types/config/router';

import Default       from '@/src/app/system/layout/default.vue';
import AppleLogin    from '@/src/app/sample/apple/view/apple-login.vue';
import AppleLoginPop from '@/src/app/sample/apple/view/apple-login-pop.vue';

const router: RouteConfig[] = [
  {
    path     : '/default/apple',
    component: Default,
    redirect : '/default/apple/login',
    children : [
      {
        path     : 'login',
        component: AppleLogin,
      },
      {
        path     : 'login-pop',
        component: AppleLoginPop,
      }
    ]
  }
];

export default router;
