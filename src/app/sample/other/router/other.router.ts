import {RouteConfig} from '@nuxt/types/config/router';

import Default from '@/src/app/system/layout/default.vue';
import OtherIndex from '@/src/app/sample/other/view/other-index.vue';
import OtherLogin from '@/src/app/sample/other/view/other-login.vue';
import OtherMiddleware from '@/src/app/sample/other/view/other-middleware.vue';
import OtherMiddlewareAfter from '@/src/app/sample/other/view/other-middleware-after.vue';
import OtherServers from '@/src/app/sample/other/view/other-servers.vue';
import OtherSwiper from '@/src/app/sample/other/view/other-swiper.vue';


const router: RouteConfig[] = [
  {
    path: '/default/other',
    component: Default,
    redirect: '/default/other/index',
    children: [
      {
        path: 'index',
        component: OtherIndex,
      },
      {
        path: 'login',
        component: OtherLogin,
      },
      {
        path: 'middleware',
        component: OtherMiddleware,
      },
      {
        path: 'middleware-after',
        component: OtherMiddlewareAfter,
      },
      {
        path: 'server',
        component: OtherServers,
      },
      {
        path: 'swiper',
        component: OtherSwiper,
      }
    ]
  }
];

export default router;
