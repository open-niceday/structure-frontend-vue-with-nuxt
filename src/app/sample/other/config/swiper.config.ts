import {SwiperOptions} from 'swiper';

export const SAMPLE_SWIPER_ARRAY = [
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
];

export const SWIPER_SINGLE_PAGINATION: SwiperOptions =  {
  slidesPerView: 1,
  spaceBetween: 30,
  loop: true,
  autoplay: {
    delay: 2500,
    disableOnInteraction: false
  },
  pagination: {
    el: '.swiper-pagination'
  }
};

export const SWIPER_MULTI_NAVIGATION: SwiperOptions =  {
  slidesPerView: 3,
  spaceBetween: 30,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev'
  },
  breakpoints: {
    1024: {
      slidesPerView: 3,
      spaceBetween: 40
    },
    768: {
      slidesPerView: 2,
      spaceBetween: 30
    }
  }
};

export const SWIPER_3D_SCROLLBAR: SwiperOptions =  {
  slidesPerView: 5,
  spaceBetween: 30,
  loop: true,
  effect: 'coverflow',
  grabCursor: true,
  centeredSlides: true,
  // slidesPerView: 'auto',
  coverflowEffect: {
    rotate: 10,
    stretch: 0,
    depth: 100,
    modifier: 1,
    slideShadows: true
  },
  autoplay: {
    delay: 2500,
    disableOnInteraction: false
  },
  scrollbar: {
    el: '.swiper-scrollbar',
    draggable: true,
  }
};
