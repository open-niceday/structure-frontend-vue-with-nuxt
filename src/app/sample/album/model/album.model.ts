import 'reflect-metadata';
import {Expose} from 'class-transformer';
import {IsNotEmpty, IsNumber, IsString} from 'class-validator';

import {Description} from '@/src/core/decorator/description.decorator';

export namespace Album {
  export namespace Request {
    export class Add {
      @Expose() @Description('사용자-아이디')
      @IsNumber() @IsNotEmpty()
      userId!: number;

      @Expose() @Description('제목')
      @IsString() @IsNotEmpty()
      title!: string;
    }
  }

  export namespace Response {
    export class FindAll {
      @Expose() @Description('사용자-아이디')
      userId!: number;

      @Expose() @Description('아이디')
      id!: number;

      @Expose() @Description('제목')
      title!: string;
    }

    export class FindOne {
      @Expose() @Description('사용자-아이디')
      userId!: number;

      @Expose() @Description('아이디')
      id!: number;

      @Expose() @Description('제목')
      title!: string;
    }
  }
}
