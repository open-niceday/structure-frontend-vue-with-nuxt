import {AxiosError, AxiosRequestConfig, AxiosResponse} from 'axios';

export class AxiosProcessorService {
  onRequest(config: AxiosRequestConfig): AxiosRequestConfig {
    return config;
  }

  onResponse(response: AxiosResponse): string {

    return 'message';
  }

  onError(error: AxiosError): string {

    return 'message';
  }

  onRequestError(error: AxiosError): string {
    return 'message';
  }

  onResponseError(error: AxiosError): string {
    return 'message';
  }
}
