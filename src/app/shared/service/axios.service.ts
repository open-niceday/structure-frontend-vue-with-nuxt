import Axios, {AxiosInstance, AxiosRequestConfig, AxiosResponse, AxiosError} from 'axios'
import qs from 'qs';

import {Enum} from '@/src/app/shared/model/enum.model';
import {Broadcast} from '@/src/core/service/broadcast.service';
import {AxiosProcessorService} from '@/src/app/shared/service/axios.processor.service';

export default class AxiosService {
  private axios: AxiosInstance;
  private broadcast = new Broadcast();
  private axiosProcessorService = new AxiosProcessorService();

  constructor(baseURL?: string) {
    const timeout = 1000 * 60;
    this.axios = Axios.create({
      baseURL: !!baseURL ? baseURL : '',
      timeout,
      paramsSerializer: (params) => qs.stringify(params, {encode: false, allowDots: true}),
    });

    this.axios.interceptors.request.use(
      (config: AxiosRequestConfig) => {
        if (process.client) {
          config = this.axiosProcessorService.onRequest(config);

          this.broadcast.onSpinner(Enum.CORE.SPINNER.DEFAULT, true);
        }

        return config;
      },
      (error: AxiosError) => {
        if (process.client) {
          const message = this.axiosProcessorService.onRequestError(error);
          console.log(message);

          this.broadcast.onError(error);
        }

        return Promise.reject(error);
      }
    );

    this.axios.interceptors.response.use(
      (response: AxiosResponse) => {
        if (process.client) {
          const message = this.axiosProcessorService.onResponse(response);
          console.log(message);

          this.broadcast.onSpinner(Enum.CORE.SPINNER.DEFAULT, false);
        }

        return response;
      },
      (error: AxiosError) => {
        if (process.client) {
          const message = this.axiosProcessorService.onRequestError(error);
          console.log(message);

          this.broadcast.onError(error);
        }

        return Promise.reject(error);
      }
    );
  }

  get(url: string, options?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.get(url, options);
  }

  delete(url: string, options?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.delete(url, options);
  }

  post(url: string, params?: any, options?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.post(url, params, options);
  }

  put(url: string, params?: any, options?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.put(url, params, options);
  }

  patch(url: string, params?: any, options?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.patch(url, params, options);
  }
}
