import Cookies from 'js-cookie';
import {ClassConstructor} from 'class-transformer';

import {Enum} from '@/src/app/shared/model/enum.model';
import {Mapper} from '@/src/core/service/mapper.service';

export default class StorageService {
  mapper = new Mapper();

  removeLocalStorage(key: Enum.CORE.STORAGE.KEY) {
    if (process.browser) {
      localStorage.removeItem(key);
    }
  }

  setLocalStorage(key: Enum.CORE.STORAGE.KEY, params: any) {
    if (process.browser) {
      if (this.isObject(params)) {
        localStorage.setItem(key, JSON.stringify(params));
      } else {
        localStorage.setItem(key, params);
      }
    }
  }

  getLocalStorage<T>(key: Enum.CORE.STORAGE.KEY, type?: Enum.CORE.STORAGE.TYPE, classType?: ClassConstructor<T>): any {
    if (process.browser) {
      const params = localStorage.getItem(key);
      return this.getStorage<T>(params, type, classType);
    }
  }

  removeCookie(key: Enum.CORE.STORAGE.KEY) {
    Cookies.remove(key);
  }

  setCookie(key: Enum.CORE.STORAGE.KEY, params: any) {
    if (this.isObject(params)) {
      Cookies.set(key, JSON.stringify(params));
    } else {
      Cookies.set(key, params);
    }
  }

  getCookie<T>(key: Enum.CORE.STORAGE.KEY, type?: Enum.CORE.STORAGE.TYPE, classType?: ClassConstructor<T>): any {
    const params = Cookies.get(key);
    return this.getStorage<T>(params, type, classType);
  }

  private isObject(params: any): boolean {
    let returnValue: boolean = false;

    if (typeof params === Enum.CORE.STORAGE.TYPE.OBJECT) {
      returnValue = true;
    }

    return returnValue;
  }

  private getStorage<T>(params: any, type?: Enum.CORE.STORAGE.TYPE, classType?: ClassConstructor<T>): any {
    let returnValue: any = null;

    switch (type) {
      case Enum.CORE.STORAGE.TYPE.STRING:
        returnValue = params;
        break;
      case Enum.CORE.STORAGE.TYPE.NUMBER:
        returnValue = parseInt(params, 10);
        break;
      case Enum.CORE.STORAGE.TYPE.BOOLEAN:
        switch (params) {
          case 'false':
            returnValue = false;
            break;
          case 'true':
            returnValue = true;
            break;
        }
        break;
      case Enum.CORE.STORAGE.TYPE.OBJECT:
        returnValue = !!classType ? this.mapper.toObject<T>(classType, JSON.parse(params)) : JSON.parse(params);
        break;
      case Enum.CORE.STORAGE.TYPE.ARRAY:
        returnValue = !!classType ? this.mapper.toArray(classType, JSON.parse(params)) : JSON.parse(params);
        break;
      default:
        returnValue = params;
        break;
    }

    return returnValue;
  }
}
