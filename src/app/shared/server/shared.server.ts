export namespace SharedServer {
  export const Index = {
    async asyncData({store}: any) {
      await store.dispatch('global/setToken', 'TEST_GLOBAL_SAMPLE');

      console.log('SERVER GLOBAL: ', store.getters['global/getToken']());
      return {};
    }
  };

  export const RouterGuard = {
    middleware: ['check.middleware']
  }

  export const Server = {
    async asyncData({store}: any) {
      try {
        await store.dispatch('user/getUsers');
        console.log('SERVER GLOBAL: ', store.getters['global/getToken']());
        await store.dispatch('user/getUser', '1');
      } catch (error) {
        console.log('ERROR', error);
      }

      return {
        users: store.getters['user/getUsers'](),
        user: store.getters['user/getUser']()
      };
    }
  }
}
